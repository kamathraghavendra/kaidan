<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="it">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Un client Jabber/XMPP semplice e intuitivo</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licenza:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Visualizza il codice sorgente online</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation>Segnala problemi</translation>
    </message>
</context>
<context>
    <name>AccountSecurity</name>
    <message>
        <source>Account Security</source>
        <translation>Sicurezza dell&apos;account</translation>
    </message>
    <message>
        <source>Allow to add additional devices using the login QR code but never show the password.</source>
        <translation>Permetti di aggiungere altri dispositivi usando il codice QR di accesso ma non mostrare mai la password.</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Non mostrare la password</translation>
    </message>
    <message>
        <source>Neither allow to add additional devices using the login QR code nor show the password.</source>
        <translation>Non permettere di aggiungere altri dispositivi usando il codice QR di accesso né mostrare la password.</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation>Non esporre la password in alcun modo</translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation>Trasferisci l&apos;account su un altro dispositivo</translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation>Scannerizza il codice QR o inserisci le credenziali come testo su un altro dispositivo per accedere su di esso.

Attenzione:
Non mostrare mai questo codice QR a nessun altro. Permetterebbe un accesso illimitato al tuo account!</translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation>Indirizzo della discussione:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Password:</translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation>Nascondi il codice QR</translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation>Mostra come codice QR</translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation>Nascondi il testo</translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation>Mostra come testo</translation>
    </message>
    <message>
        <source>Copy password</source>
        <translation>Copia la password</translation>
    </message>
    <message>
        <source>Copy chat address</source>
        <translation>Copia l&apos;indirizzo della discussione</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Cambia la password</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Password attuale:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Nuova password:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Nuova password (ripeti):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Le nuove password non corrispondono.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>La password attuale non è corretta.</translation>
    </message>
    <message>
        <source>You need to be connected to change your password.</source>
        <translation type="vanished">Devi essere connesso per cambiare la tua password.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Dopo aver cambiato la tua password dovrai reinserirla in tutti i tuoi altri dispositivi.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
    <message>
        <source>Password changed successfully</source>
        <translation>La password è stata cambiata correttamente</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Impossibile cambiare la password: %1</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Copy Message</source>
        <translation type="vanished">Copia il messaggio</translation>
    </message>
    <message>
        <source>Edit Message</source>
        <translation type="vanished">Modifica il messaggio</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Scarica</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation type="vanished">Copia URL di download</translation>
    </message>
    <message>
        <source>Quote</source>
        <translation type="vanished">Cita</translation>
    </message>
</context>
<context>
    <name>ChatMessageContextMenu</name>
    <message>
        <source>Copy message</source>
        <translation>Copia il messaggio</translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation>Modifica il messaggio</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation>Copia l&apos;URL di scaricamento</translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation>Cita il messaggio</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Compose message</source>
        <translation type="vanished">Componi il messaggio</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="vanished">Immagine</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation type="vanished">Documento</translation>
    </message>
    <message>
        <source>Other file</source>
        <translation type="vanished">Altro file</translation>
    </message>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Seleziona un file</translation>
    </message>
    <message>
        <source>Send a spoiler message</source>
        <translation>Invia un messaggio con spoiler</translation>
    </message>
    <message>
        <source>Spoiler hint</source>
        <translation type="vanished">Accenno dello spoiler</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation>Riattiva notifiche</translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation>Silenzia notifiche</translation>
    </message>
    <message>
        <source>View profile</source>
        <translation>Visualizza profilo</translation>
    </message>
    <message>
        <source>Multimedia settings</source>
        <translation type="vanished">Impostazioni multimediali</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Cerca</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Chiudi</translation>
    </message>
    <message>
        <source>Search up</source>
        <translation>Cerca verso l&apos;alto</translation>
    </message>
    <message>
        <source>Search down</source>
        <translation>Cerca verso il basso</translation>
    </message>
    <message>
        <source>All files</source>
        <translation type="vanished">Tutti i file</translation>
    </message>
    <message>
        <source>Close message search bar</source>
        <translation>Chiudi la barra di ricerca dei messaggi</translation>
    </message>
</context>
<context>
    <name>ChatPageSendingPane</name>
    <message>
        <source>Spoiler hint</source>
        <translation>Accenno dello spoiler</translation>
    </message>
    <message>
        <source>Close spoiler hint field</source>
        <translation>Chiudi il campo di avviso sul contenuto</translation>
    </message>
    <message>
        <source>Compose message</source>
        <translation>Componi un messaggio</translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation>Non è stato possibile eliminare il tuo account dal server. Pertanto, non è stato rimosso anche da questa applicazione: %1</translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation>Molto basso</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Basso</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Normale</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Alto</translation>
    </message>
    <message>
        <source>Very high</source>
        <translation>Molto alto</translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation>Qualità costante</translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation>Bit rate costante</translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation>Bit rate medio</translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation>Due passaggi</translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
</context>
<context>
    <name>ConnectionSettings</name>
    <message>
        <source>Connection settings</source>
        <translation>Impostazioni di connessione</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Cambia</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Connection settings could not be changed</source>
        <translation>Le impostazioni di connessione non possono essere modificate</translation>
    </message>
    <message>
        <source>Connection settings changed</source>
        <translation>Impostazioni di connessione cambiate</translation>
    </message>
</context>
<context>
    <name>CustomConnectionSettings</name>
    <message>
        <source>Hostname:</source>
        <translation>Nome dell&apos;host:</translation>
    </message>
    <message>
        <source>The hostname must not contain blank spaces</source>
        <translation>Il nome dell&apos;host non deve contenere spazi vuoti</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Porta:</translation>
    </message>
</context>
<context>
    <name>DisablePasswordDisplay</name>
    <message>
        <source>Don&apos;t expose your password in any way</source>
        <translation>Non esporre la tua password in nessun modo</translation>
    </message>
    <message>
        <source>Your password will neither be shown as plain text nor included in the login QR code anymore.
You won&apos;t be able to use the login via QR code without entering your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device.</source>
        <translation>La tua password non sarà più mostrata come testo semplice né inclusa nel codice QR di accesso.
Non sarai più in grado di effettuare l&apos;accesso tramite codice QR senza inserire nuovamente la tua password perché questa azione non può essere annullata!
Considera di memorizzare la password da qualche altra parte se vuoi usare il tuo account su un altro dispositivo.</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation>Non esporre la password in alcun modo</translation>
    </message>
</context>
<context>
    <name>DisablePlainTextPasswordDisplay</name>
    <message>
        <source>Don&apos;t show your password</source>
        <translation>Non mostrare la tua password</translation>
    </message>
    <message>
        <source>Your password will not be shown anymore but still exposed via the login QR code.
You won&apos;t be able to see your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device without the login QR code.</source>
        <translation>La tua password non sarà più mostrata, ma ancora esposta tramite il codice QR di accesso.
Non sarai più in grado di vedere la tua password perché questa azione non può essere annullata!
Considera di memorizzare la password da qualche altra parte se vuoi usare il tuo account su un altro dispositivo senza il codice QR di accesso.</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Non mostrare la password</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Impossibile salvare il file: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Scaricamento fallito: %1</translation>
    </message>
</context>
<context>
    <name>EmojiPicker</name>
    <message>
        <source>Favorites</source>
        <translation type="vanished">Preferiti</translation>
    </message>
    <message>
        <source>People</source>
        <translation type="vanished">Persone</translation>
    </message>
    <message>
        <source>Nature</source>
        <translation type="vanished">Natura</translation>
    </message>
    <message>
        <source>Food</source>
        <translation type="vanished">Cibo</translation>
    </message>
    <message>
        <source>Activity</source>
        <translation type="vanished">Attività</translation>
    </message>
    <message>
        <source>Travel</source>
        <translation type="vanished">Viaggi</translation>
    </message>
    <message>
        <source>Objects</source>
        <translation type="vanished">Oggetti</translation>
    </message>
    <message>
        <source>Symbols</source>
        <translation type="vanished">Simboli</translation>
    </message>
    <message>
        <source>Flags</source>
        <translation type="vanished">Bandiere</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Cerca</translation>
    </message>
    <message>
        <source>Search emoji</source>
        <translation type="vanished">Cerca emoji</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Seleziona una conversazione per iniziare a messaggiare</translation>
    </message>
</context>
<context>
    <name>FileChooser</name>
    <message>
        <source>Select a file</source>
        <translation type="vanished">Seleziona un file</translation>
    </message>
</context>
<context>
    <name>FileChooserMobile</name>
    <message>
        <source>Go to parent folder</source>
        <translation type="vanished">Vai al livello superiore</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Chiudi</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>Log out</source>
        <translation type="vanished">Esci</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Invita amici</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Collegamento dell&apos;invito copiato negli appunti</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Fuori linea</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>In linea</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>Switch device</source>
        <translation>Cambia dispositivo</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Il collegamento verrà aperto appena ti sarai connesso.</translation>
    </message>
    <message>
        <source>No password found. Please enter it.</source>
        <translation type="vanished">Nessuna password trovata. Per favore inseriscila.</translation>
    </message>
    <message>
        <source>No valid login QR code found.</source>
        <translation type="vanished">Nessun codice QR di accesso valido trovato.</translation>
    </message>
</context>
<context>
    <name>LocalAccountRemoval</name>
    <message>
        <source>Remove account from this app</source>
        <translation>Rimuovi l&apos;account da questa applicazione</translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
Your password will be deleted, make sure it is stored in a password manager or you know it!</source>
        <translation>Il tuo account sarà rimosso da questa applicazione.
La tua password sarà eliminata, assicurati che sia memorizzata in un gestore di password o che tu la conosca!</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Accedi</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Accedi al tuo account XMPP</translation>
    </message>
    <message>
        <source>Your Jabber-ID:</source>
        <translation type="vanished">Il tuo Jabber-ID:</translation>
    </message>
    <message>
        <source>Your diaspora*-ID:</source>
        <translation type="vanished">Il tuo diaspora*-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation type="vanished">nome@esempio.org</translation>
    </message>
    <message>
        <source>Your Password:</source>
        <translation type="vanished">La tua password:</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">Connetti</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation type="vanished">Nome utente o password non validi.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation type="vanished">Impossibile connettersi al server. Per favore controlla la tua connessione a internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation type="vanished">Il server non supporta connessioni sicure.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation type="vanished">Errore durante il tentativo di connessione sicura.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your JID again.</source>
        <translation type="vanished">Impossibile risolvere l&apos;indirizzo del server. Per favore, controlla di nuovo il tuo JID.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Non posso connettermi al server.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="vanished">Protocollo di autenticazione non supportato dal server.</translation>
    </message>
    <message>
        <source>An unknown error occured; see log for details.</source>
        <translation type="vanished">Si è verificato un errore sconosciuto; per i dettagli guarda il registro.</translation>
    </message>
    <message>
        <source>Log in using a QR-Code</source>
        <translation type="vanished">Accedi utilizzando un QR-Code</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Impostazioni di connessione</translation>
    </message>
</context>
<context>
    <name>LoginSettingsPage</name>
    <message>
        <source>Connection settings</source>
        <translation>Impostazioni di connessione</translation>
    </message>
    <message>
        <source>The connection settings will be saved permanently after the first successful login. If they don&apos;t work, you&apos;ll get back to the login page.</source>
        <translation>Le impostazioni di connessione saranno salvate permanentemente dopo il primo accesso riuscito. Se non funzionano, tornerai alla pagina di accesso.</translation>
    </message>
    <message>
        <source>Enable custom connection settings</source>
        <translation>Abilita le impostazioni di connessione personalizzate</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Porta:</translation>
    </message>
    <message>
        <source>Hostname:</source>
        <translation>Nome dell&apos;host:</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Ripristina</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Applica</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation>Predefinito</translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation>Scatta foto</translation>
    </message>
    <message>
        <source>Record video</source>
        <translation>Registra video</translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation>Registra audio</translation>
    </message>
    <message>
        <source>Send location</source>
        <translation>Invia posizione</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Scegli un file</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>Scegli un&apos;immagine</translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation>Scegli un video</translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation>Scegli un file audio</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>Scegli un documento</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Tutti i file</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Immagini</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation>File audio</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation>Documenti</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Documento</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Posizione</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>File</source>
        <translation>File</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>Spoiler</source>
        <translation>Spoiler</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Could not send message, as a result of not being connected.</source>
        <translation type="vanished">Impossibile inviare messaggi, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Could not correct message, as a result of not being connected.</source>
        <translation type="vanished">Impossibile correggere il messaggio, perchè non sei connesso.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="vanished">Spoiler</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Il messaggio non può essere inviato.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>La correzione del messaggio non è riuscita.</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <source>Pending</source>
        <translation>In attesa</translation>
    </message>
    <message>
        <source>Sent</source>
        <translation>Inviato</translation>
    </message>
    <message>
        <source>Delivered</source>
        <translation>Consegnato</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Errore</translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation>Impostazioni multimediali</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation>Configura</translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation>Cattura immagine</translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation>Registrazione audio</translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation>Registrazione video</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation>Fotocamera</translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation>Ingresso audio</translation>
    </message>
    <message>
        <source>Container</source>
        <translation>Contenitore</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Immagine</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>Codec</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Risoluzione</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation>Qualità</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation>Frequenza di campionamento</translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation>Frequenza fotogrammi</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Pronto</translation>
    </message>
    <message>
        <source>Initializing…</source>
        <translation>Inizializzazione…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Non disponibile</translation>
    </message>
    <message>
        <source>Recording…</source>
        <translation>Registrazione…</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>In pausa</translation>
    </message>
</context>
<context>
    <name>MultimediaSettingsPage</name>
    <message>
        <source>Image</source>
        <translation type="vanished">Immagine</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="vanished">Audio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">Video</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Ready</source>
        <translation type="vanished">Pronto</translation>
    </message>
    <message>
        <source>Initializing…</source>
        <translation>Inizializzazione…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Non disponibile</translation>
    </message>
    <message>
        <source>Recording… %1</source>
        <translation>Registrazione… %1</translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation>In pausa %1</translation>
    </message>
</context>
<context>
    <name>PasswordRemovalPage</name>
    <message>
        <source>Remove password</source>
        <translation>Rimuovi la password</translation>
    </message>
    <message>
        <source>You can decide to only not show your password for &lt;b&gt;%1&lt;/b&gt; as text anymore or to remove it completely from the account transfer. If you remove your password completely, you won&apos;t be able to use the account transfer via scanning without entering your password because it is also removed from the QR code.</source>
        <translation>Puoi decidere di non mostrare più la tua password per &lt;b&gt;%1&lt;/b&gt; come testo o di rimuoverla completamente dal trasferimento dell&apos;account. Se rimuovi completamente la password, non sarai in grado di utilizzare il trasferimento dell&apos;account tramite scansione senza inserire la password perché questa viene rimossa anche dal codice QR.</translation>
    </message>
    <message>
        <source>Do not show password as text</source>
        <translation>Non mostrare la password come testo</translation>
    </message>
    <message>
        <source>Remove completely</source>
        <translation>Rimuovi completamente</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Mark as read</source>
        <translation>Segna come letto</translation>
    </message>
    <message>
        <source>Available</source>
        <translation>Disponibile</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation>Disponibile a conversare</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Assente</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation>Non disturbare</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation>Assente per molto tempo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Fuori linea</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Available</source>
        <translation type="vanished">Disponibile</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Disponibile a conversare</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Assente</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Non disturbare</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Lontano per molto tempo</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation type="vanished">Non in linea</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Errore</translation>
    </message>
    <message>
        <source>Invisible</source>
        <translation type="vanished">Invisibile</translation>
    </message>
    <message>
        <source>Invalid username or password.</source>
        <translation>Nome utente o password non validi.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Impossibile connettersi al server. Per favore controlla la tua connessione a internet.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Il server non supporta connessioni sicure.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Errore durante il tentativo di connessione sicura.</translation>
    </message>
    <message>
        <source>Could not resolve the server&apos;s address. Please check your server name.</source>
        <translation type="vanished">Non è stato possibile trovare l&apos;indirizzo del server. Si prega di controllare il nome del server.</translation>
    </message>
    <message>
        <source>Could not connect to the server.</source>
        <translation type="vanished">Impossibile connettersi al server.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Protocollo di autenticazione non supportato dal server.</translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation>Questo server non supporta la registrazione.</translation>
    </message>
    <message>
        <source>An unknown error occured.</source>
        <translation type="vanished">Si è verificato un errore sconosciuto.</translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation>Il server è fuori linea o bloccato da un firewall.</translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation>Non è stato possibile aggiornare la connessione.</translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation>L&apos;accesso a internet non è permesso. Controlla la configurazione di accesso a internet del tuo sistema.</translation>
    </message>
    <message>
        <source>Could not connect to the server. Please check your internet connection or your server name.</source>
        <translation>Impossibile connettersi al server. Per favore controlla la tua connessione internet o il nome del tuo server.</translation>
    </message>
    <message>
        <source>%1 is online</source>
        <translation>%1 è in linea</translation>
    </message>
    <message>
        <source>%1 is typing…</source>
        <translation>%1 sta digitando…</translation>
    </message>
    <message>
        <source>%1 paused typing</source>
        <translation>%1 ha smesso di digitare</translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation>Generazione del codice QR fallita: %1</translation>
    </message>
</context>
<context>
    <name>QrCodeOnboardingPage</name>
    <message>
        <source>Scan QR code</source>
        <translation>Scansiona il codice QR</translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation>Mostra la spiegazione</translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation>Continua senza codice QR</translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation>Scannerizza il codice QR dal tuo dispositivo esistente per trasferire il tuo account.</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>This QR code is not a valid login QR code.</source>
        <translation>Questo codice QR non è un codice QR valido per l&apos;accesso.</translation>
    </message>
</context>
<context>
    <name>QrCodeScanner</name>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation>Il formato della fotocamera «%1» non è supportato.</translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation>Non c&apos;è nessuna fotocamera disponibile.</translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation>La fotocamera è occupata.
Prova a chiudere altre applicazioni che stanno utilizzando la fotocamera.</translation>
    </message>
</context>
<context>
    <name>QrCodeScannerPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="vanished">Scansiona il codice QR</translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation type="vanished">Non c&apos;è nessuna fotocamera disponibile.</translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation type="vanished">La fotocamera è occupata.
Provare a chiudere altre applicazioni che stanno utilizzando la fotocamera.</translation>
    </message>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation type="vanished">Il formato della fotocamera «%1» non è supportato.</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation type="obsolete">Connessione…</translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation>Configurazione</translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation>Registra un nuovo account</translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation>Usa un account esistente</translation>
    </message>
</context>
<context>
    <name>RegistrationManager</name>
    <message>
        <source>Password changed successfully.</source>
        <translation type="vanished">La password è stata cambiata correttamente.</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation type="vanished">Cambio password non riuscito: %1</translation>
    </message>
</context>
<context>
    <name>RemoteAccountDeletion</name>
    <message>
        <source>Delete account completely</source>
        <translation>Elimina completamente l&apos;account</translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation>Il tuo account sarà eliminato completamente, cioè da questa applicazione e dal server.
Non sarai più in grado di utilizzare il tuo account!</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Aggiungi nuovo contatto</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>nome@esempio.org</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Questo invierà anche una richiesta di accesso allo stato di presenza del contatto.</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Soprannome:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Messaggio opzionale:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Scrivi al contatto della conversazione chi sei.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Aggiungi</translation>
    </message>
</context>
<context>
    <name>RosterListItem</name>
    <message>
        <source>Offline</source>
        <translation type="vanished">Non in linea</translation>
    </message>
    <message>
        <source>Error: Please check the JID.</source>
        <translation type="vanished">Errore: per favore controlla il JID.</translation>
    </message>
    <message>
        <source>Available</source>
        <translation type="vanished">Disponibile</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation type="vanished">Disponibile a conversare</translation>
    </message>
    <message>
        <source>Away</source>
        <translation type="vanished">Lontano dal dispositivo</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation type="vanished">Non disturbare</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation type="vanished">Lontano per tanto tempo</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">Errore</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Impossibile aggiungere il contatto, perché non sei connesso.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Impossibile rimuovere il contatto, perché non sei connesso.</translation>
    </message>
    <message>
        <source>Spoiler</source>
        <translation type="vanished">Spoiler</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation>Impossibile rinominare il contatto, perchè non sei connesso.</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Connessione…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Contatti</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Aggiungi nuovo contatto</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Fuori linea</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation>Cerca contatti</translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your roster?</source>
        <translation type="vanished">Vuoi veramente eliminare il contatto &lt;b&gt;%1&lt;/b&gt; dalla tua lista?</translation>
    </message>
    <message>
        <source>Delete contact</source>
        <translation>Elimina il contatto</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Elimina</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your contact list?</source>
        <translation>Vuoi davvero eliminare il contatto &lt;b&gt;%1&lt;/b&gt; dalla tua lista di contatti?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation>Rinomina il contatto</translation>
    </message>
    <message>
        <source>Edit name:</source>
        <translation type="vanished">Modifica il nome:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Rinomina</translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Descrizione</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Annulla</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Invia</translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation>Server personalizzato</translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation>Nessuna limitazione</translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation>%1 giorni</translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation>Cambia la password</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Cambia la password del tuo account. Dovrai reinserirla negli altri tuoi dispositivi.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation>Impostazioni multimediali</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation>Configura le impostazioni di registrazione di foto, video e audio</translation>
    </message>
    <message>
        <source>Account security</source>
        <translation>Sicurezza dell&apos;account</translation>
    </message>
    <message>
        <source>Remove account from Kaidan</source>
        <translation>Rimuovi l&apos;account da Kaidan</translation>
    </message>
    <message>
        <source>Remove account from this app</source>
        <translation>Rimuovi l&apos;account da questa applicazione</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Elimina l&apos;account</translation>
    </message>
    <message>
        <source>Delete account from the server</source>
        <translation>Elimina l&apos;account dal server</translation>
    </message>
    <message>
        <source>Connection Settings</source>
        <translation>Impostazioni di connessione</translation>
    </message>
    <message>
        <source>Configure the hostname and port to connect to</source>
        <translation>Configura il nome dell&apos;host e la porta a cui connettersi</translation>
    </message>
    <message>
        <source>Configure whether this device can be used to log in on another device</source>
        <translation>Configura se questo dispositivo può essere usato per accedere a un altro dispositivo</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Impostazioni</translation>
    </message>
    <message>
        <source>Change password</source>
        <translation type="vanished">Cambia la password</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation type="vanished">Cambia la password del tuo account. Dovrai reinserirla negli altri tuoi dispositivi.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation type="vanished">Impostazioni multimediali</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation type="vanished">Configura le impostazioni di registrazione di foto, video e audio</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation>Goditi la comunicazione libera su ogni dispositivo!</translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation>Iniziamo</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Richiesta di sottoscrizione</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Hai ricevuto una richiesta di sottoscrizione da &lt;b&gt;%1&lt;/b&gt;. Se la accetti, l&apos;account avrà accesso al tuo stato di presenza.</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Rifiuta</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Accetta</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Impossibile inviare il file, in quanto non connesso.</translation>
    </message>
    <message>
        <source>File</source>
        <translation type="vanished">File</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Il messaggio non ha potuto essere inviato.</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation>Profilo</translation>
    </message>
    <message>
        <source>Rename contact</source>
        <translation>Rinomina il contatto</translation>
    </message>
    <message>
        <source>Show QR code</source>
        <translation>Mostra il codice QR</translation>
    </message>
    <message>
        <source>Remove contact</source>
        <translation>Rimuovi il contatto</translation>
    </message>
    <message>
        <source>Online devices</source>
        <translation>Dispositivi in linea</translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation>Nome</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Soprannome</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Informazioni</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-mail</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Compleanno</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Sito web</translation>
    </message>
</context>
</TS>
