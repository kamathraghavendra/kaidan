<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="sl">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Enostaven, uporabniku prijazen Jabber/XMPP odjemalec</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Licenca:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Prikaži izvorno kodo na spletu</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation>Prijavi težave</translation>
    </message>
</context>
<context>
    <name>AccountSecurity</name>
    <message>
        <source>Account Security</source>
        <translation>Varnost računa</translation>
    </message>
    <message>
        <source>Allow to add additional devices using the login QR code but never show the password.</source>
        <translation>Dovoli dodajanje dodatnih naprav z uporabo prijavne QR kode, a nikoli ne prikaži gesla.</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Ne prikaži gesla</translation>
    </message>
    <message>
        <source>Neither allow to add additional devices using the login QR code nor show the password.</source>
        <translation>Niti ne dovoli dodajanje dodatnih naprav z uporabo prijavne QR kode niti ne prikaži gesla.</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation>Prenesi račun na drugo napravo</translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation>Naslov klepeta:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Geslo:</translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation>Skrij QR kodo</translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation>Prikaži kot QR kodo</translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation>Skrij besedilo</translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation>Prikaži kot besedilo</translation>
    </message>
    <message>
        <source>Copy password</source>
        <translation>Kopiraj geslo</translation>
    </message>
    <message>
        <source>Copy chat address</source>
        <translation>Kopiraj naslov klepeta</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Sprememba gesla</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Trenutno geslo:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Novo geslo:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Novo geslo (ponovno):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Novi gesli se ne ujemata.</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Trenutno geslo je napačno.</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Po spremembi gesla ga boste morali ponovno vnesti na vseh svojih drugih napravah.</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Spremeni</translation>
    </message>
    <message>
        <source>Password changed successfully</source>
        <translation>Geslo uspešno spremenjeno</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Spreminjanje gesla spodletelo: %1</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Prenesi</translation>
    </message>
</context>
<context>
    <name>ChatMessageContextMenu</name>
    <message>
        <source>Copy message</source>
        <translation>Kopiraj sporočilo</translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation>Uredi sporočilo</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation>Kopiraj povezavo do prenosa</translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation>Citiraj sporočilo</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Send a spoiler message</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation>Vklopi obvestila</translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation>Utišaj obvestila</translation>
    </message>
    <message>
        <source>View profile</source>
        <translation>Poglej profil</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Iskanje</translation>
    </message>
    <message>
        <source>Search up</source>
        <translation>Iskanje navzgor</translation>
    </message>
    <message>
        <source>Search down</source>
        <translation>Iskanje navzdol</translation>
    </message>
    <message>
        <source>Close message search bar</source>
        <translation>Zapri vrstico iskanja po sporočilih</translation>
    </message>
</context>
<context>
    <name>ChatPageSendingPane</name>
    <message>
        <source>Spoiler hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Close spoiler hint field</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Compose message</source>
        <translation>Sestavi sporočilo</translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation>Brisanje vašega računa s strežnika ni uspelo. Zato prav tako ni bil odstranjen iz te aplikacije: %1</translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>High</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Very high</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation>Konstanta kakovost</translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation>Enakomerna bitna hitrost</translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation>Povprečna bitna hitrost</translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation>Dvakratni prehod</translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
</context>
<context>
    <name>ConnectionSettings</name>
    <message>
        <source>Connection settings</source>
        <translation>Nastavitve povezave</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Spremeni</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Connection settings could not be changed</source>
        <translation>Nastavitev povezave ni bilo mogoče spremeniti</translation>
    </message>
    <message>
        <source>Connection settings changed</source>
        <translation>Nastavitve povezave spremenjene</translation>
    </message>
</context>
<context>
    <name>CustomConnectionSettings</name>
    <message>
        <source>Hostname:</source>
        <translation>Ime strežnika:</translation>
    </message>
    <message>
        <source>The hostname must not contain blank spaces</source>
        <translation>Ime strežnika ne sme vsebovati praznih presledkov</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Vrata:</translation>
    </message>
</context>
<context>
    <name>DisablePasswordDisplay</name>
    <message>
        <source>Don&apos;t expose your password in any way</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your password will neither be shown as plain text nor included in the login QR code anymore.
You won&apos;t be able to use the login via QR code without entering your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device.</source>
        <translation>Vaše geslo ne bo več niti prikazano kot besedilo niti ne bo vključeno v prijavno QR kodo.
Ne boste se mogli prijaviti s QR kodo brez ponovnega vnosa vašega gesla, ker tega dejanja ni mogoče razveljaviti!
Priporočljivo je, da si geslo shranite nekam drugam, če želite uporabljati vaš račun na drugi napravi.</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DisablePlainTextPasswordDisplay</name>
    <message>
        <source>Don&apos;t show your password</source>
        <translation>Ne prikaži gesla</translation>
    </message>
    <message>
        <source>Your password will not be shown anymore but still exposed via the login QR code.
You won&apos;t be able to see your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device without the login QR code.</source>
        <translation>Vaše geslo ne bo več niti prikazano kot besedilo, bo pa še vedno vključeno v prijavno QR kodo.
Ne boste več mogli videti svojega gesla, ker tega dejanja ni mogoče razveljaviti!
Priporočljivo je, da si geslo shranite nekam drugam, če želite uporabljati vaš račun na drugi napravi brez prijavne QR kode.</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Ne prikaži gesla</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Ni bilo mogoče shraniti datoteke: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Prenos spodletel: %1</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Prosimo, izberite klepet, da začnete s pogovarjanem</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Povabi prijatelje</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Povezava za povabilo kopirana v odložišče</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Nepovezan</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>Povezan</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Povezovanje…</translation>
    </message>
    <message>
        <source>Switch device</source>
        <translation>Zamenjaj napravo</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Povezava se bo odprla, ko se povežete.</translation>
    </message>
</context>
<context>
    <name>LocalAccountRemoval</name>
    <message>
        <source>Remove account from this app</source>
        <translation>Odstrani račun iz te aplikacije</translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
Your password will be deleted, make sure it is stored in a password manager or you know it!</source>
        <translation>Vaš račun bo odstranjen iz te aplikacije.
Vaše geslo bo izbrisano! Prepričajte se, da je shranjeno v upravljalniku gesel ali da ga poznate!</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Odstrani</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Prijava</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Prijavite se v svoj XMPP račun</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Povezovanje…</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Nastavitve povezave</translation>
    </message>
</context>
<context>
    <name>LoginSettingsPage</name>
    <message>
        <source>Connection settings</source>
        <translation>Nastavitve povezave</translation>
    </message>
    <message>
        <source>The connection settings will be saved permanently after the first successful login. If they don&apos;t work, you&apos;ll get back to the login page.</source>
        <translation>Po prvi uspešni prijavi bodo nastavitve povezave shranjene za vedno. Če ne bodo delovale, se boste vrnili na prijavno stran.</translation>
    </message>
    <message>
        <source>Enable custom connection settings</source>
        <translation>Omogoči nastavitve povezave po meri</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Vrata:</translation>
    </message>
    <message>
        <source>Hostname:</source>
        <translation>Ime strežnika:</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Ponastavi</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Uveljavi</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation>Privzeto</translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation>Zajemi sliko</translation>
    </message>
    <message>
        <source>Record video</source>
        <translation>Zajemi videoposnetek</translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation>Zajemi zvok</translation>
    </message>
    <message>
        <source>Send location</source>
        <translation>Pošlji lokacijo</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Izberi datoteko</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>Izberi sliko</translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation>Izberi videoposnetek</translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation>Izberi zvočno datoteko</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>Izberi dokument</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Vse datoteke</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Slike</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Video datoteke</translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation>Zvočne datoteke</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation>Dokumenti</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Zvok</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Dokument</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Lokacija</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Slika</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Videoposnetek</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Datoteka</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>Spoiler</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Message could not be sent.</source>
        <translation>Pošiljanje sporočila ni uspelo.</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Popravljanje sporočila ni uspelo.</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <source>Pending</source>
        <translation>V teku</translation>
    </message>
    <message>
        <source>Sent</source>
        <translation>Poslano</translation>
    </message>
    <message>
        <source>Delivered</source>
        <translation>Dostavljeno</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Napaka</translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation>Nastavitve predstavnosti</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation>Nastavi</translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation>Zajem slik</translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation>Zajem zvoka</translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation>Zajem videoposnetkov</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation>Kamera</translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation>Zvočni vhod</translation>
    </message>
    <message>
        <source>Container</source>
        <translation>Vsebnik</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Slika</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Zvok</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>Kodek</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Ločljivost</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation>Kakovost</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation>Hitrost okvirjev</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Pripravljen</translation>
    </message>
    <message>
        <source>Initializing…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Ni na voljo</translation>
    </message>
    <message>
        <source>Recording…</source>
        <translation>Zajemanje…</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Prekinjeno</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Initializing…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Ni na voljo</translation>
    </message>
    <message>
        <source>Recording… %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation>Prekinjeno %1</translation>
    </message>
</context>
<context>
    <name>PasswordRemovalPage</name>
    <message>
        <source>Remove password</source>
        <translation>Odstrani geslo</translation>
    </message>
    <message>
        <source>You can decide to only not show your password for &lt;b&gt;%1&lt;/b&gt; as text anymore or to remove it completely from the account transfer. If you remove your password completely, you won&apos;t be able to use the account transfer via scanning without entering your password because it is also removed from the QR code.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Do not show password as text</source>
        <translation>Ne prikaži gesla kot besedilo</translation>
    </message>
    <message>
        <source>Remove completely</source>
        <translation>Popolnoma odstrani</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Mark as read</source>
        <translation>Označi kot prebrano</translation>
    </message>
    <message>
        <source>Available</source>
        <translation>Na voljo</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation>Dostopen za klepet</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Odsoten</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation>Ne moti</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation>Dlje odsoten</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Nepovezan</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Invalid username or password.</source>
        <translation>Napačno uporabniško ime ali geslo.</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Ni se mogoče povezati na strežnik. Prosimo, preverite svojo spletno povezavo.</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Strežnik ne podpira varnih povezav.</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Napaka med poskusom varne povezave.</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation>Ta strežnik ne podpira registracije.</translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation>Strežnik ni povezan ali pa je blokiran s strani požarnega zidu.</translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation>Osveževanje povezave ni uspelo.</translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation>Spletni dostop ni dovoljen. Prosimo, preverite nastavitve spletnega dostopa na vašem sistemu.</translation>
    </message>
    <message>
        <source>Could not connect to the server. Please check your internet connection or your server name.</source>
        <translation>Povezovanje na strežnik ni uspelo. Prosimo, preverite svojo spletno povezavo ali ime strežnika.</translation>
    </message>
    <message>
        <source>%1 is online</source>
        <translation>%1 je povezan</translation>
    </message>
    <message>
        <source>%1 is typing…</source>
        <translation>%1 tipka…</translation>
    </message>
    <message>
        <source>%1 paused typing</source>
        <translation>%1 je prekinil tipkanje</translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation>Ustvarjanje QR kode spodletelo: %1</translation>
    </message>
</context>
<context>
    <name>QrCodeOnboardingPage</name>
    <message>
        <source>Scan QR code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation>Prikaži pojasnilo</translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation>Nadaljuj brez QR kode</translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Povezovanje…</translation>
    </message>
    <message>
        <source>This QR code is not a valid login QR code.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QrCodeScanner</name>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation>Format kamere &apos;%1&apos; ni podprt.</translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation>Nobene kamere ni na voljo.</translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation>Vaša kamera je zaposlena.
Poskusite zapreti druge aplikacije, ki uporabljajo kamero.</translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation>Ustvari nov račun</translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation>Uporabi obstoječ račun</translation>
    </message>
</context>
<context>
    <name>RemoteAccountDeletion</name>
    <message>
        <source>Delete account completely</source>
        <translation>Popolnoma izbriši račun</translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation>Vaš račun bo popolnoma odstranjen, tako iz te aplikacije kot iz strežnika.
Svojega računa ne boste več mogli uporabljati!</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Izbriši</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Dodaj nov stik</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>uporabnik@primer.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Vzdevek:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Dodaj</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Dodajanje stika ni uspelo, ker ste nepovezani.</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Odstranjevanje stika ni uspelo, ker ste nepovezani.</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation>Preimenovanje stika ni uspelo, ker ste nepovezani.</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Povezovanje…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Stiki</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Dodaj nov stik</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Nepovezan</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation>Iskanje stikov</translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Delete contact</source>
        <translation>Izbriši stik</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Izbriši</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your contact list?</source>
        <translation>Ali res želite izbrisati stik &lt;b&gt;%1&lt;/b&gt; iz svojega seznama stikov?</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation>Preimenuj stik</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Preimenuj</translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Prekliči</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Pošlji</translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation>Strežnik po meri</translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation>Brez omejitve</translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation>%1 dni</translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Change password</source>
        <translation>Sprememba gesla</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Spremeni geslo vašega računa. Ponovno ga boste morali vnesti na vaših drugih napravah.</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation>Nastavitve predstavnosti</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation>Spremeni nastavitve zajemanja slik, videa in zvoka</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
    <message>
        <source>Account security</source>
        <translation>Varnost računa</translation>
    </message>
    <message>
        <source>Remove account from Kaidan</source>
        <translation>Odstrani račun iz Kaidan</translation>
    </message>
    <message>
        <source>Remove account from this app</source>
        <translation>Odstrani račun iz te aplikacije</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Izbriši račun</translation>
    </message>
    <message>
        <source>Delete account from the server</source>
        <translation>Izbriši račun iz strežnika</translation>
    </message>
    <message>
        <source>Connection Settings</source>
        <translation>Nastavive povezave</translation>
    </message>
    <message>
        <source>Configure the hostname and port to connect to</source>
        <translation>Nastavite ime strežnika in vrata za povezavo</translation>
    </message>
    <message>
        <source>Configure whether this device can be used to log in on another device</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Nastavitve</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation>Uživajte v prostem komuniciranju na vsaki napravi!</translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation>Začnimo</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Prošnja za naročilo</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Zavrni</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Sprejmi</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Pošiljanje datoteke ni uspelo, ker ste nepovezani.</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Pošiljanje sporočila ni uspelo.</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation>Profil</translation>
    </message>
    <message>
        <source>Rename contact</source>
        <translation>Preimenuj stik</translation>
    </message>
    <message>
        <source>Show QR code</source>
        <translation>Prikaži QR kodo</translation>
    </message>
    <message>
        <source>Remove contact</source>
        <translation>Odstrani stik</translation>
    </message>
    <message>
        <source>Online devices</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation>Ime</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Vzdevek</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="unfinished">Opis</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>E-poštni naslov</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Rojstni dan</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Spletni naslov</translation>
    </message>
</context>
</TS>
