<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="hy">
<context>
    <name>AboutDialog</name>
    <message>
        <source>A simple, user-friendly Jabber/XMPP client</source>
        <translation>Հարմարավետ և պարզ Jabber/XMPP սպասառու</translation>
    </message>
    <message>
        <source>License:</source>
        <translation>Լիցենզիա:</translation>
    </message>
    <message>
        <source>View source code online</source>
        <translation>Առցանց զննել ելակետային կոդը</translation>
    </message>
    <message>
        <source>Report problems</source>
        <translation>Տեղեկացնել խնդիրների մասին</translation>
    </message>
</context>
<context>
    <name>AccountSecurity</name>
    <message>
        <source>Account Security</source>
        <translation>Հաշվի Անվտանգություն</translation>
    </message>
    <message>
        <source>Allow to add additional devices using the login QR code but never show the password.</source>
        <translation>Թույլ տալ այլ սարքերի մուտքը QR կոդով՝ միշտ թաքցնելով գաղտնաբառը։</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Թաքցնել գաղտնաբառը</translation>
    </message>
    <message>
        <source>Neither allow to add additional devices using the login QR code nor show the password.</source>
        <translation>Չթույլատրել այլ սարքերի ավելացումը գաղտնաբառի կամ QR կոդի միջոցով։</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation>Թաքցնել գաղտնաբառը ցանկացած դեպքում</translation>
    </message>
</context>
<context>
    <name>AccountTransferPage</name>
    <message>
        <source>Transfer account to another device</source>
        <translation>Փոխանցել հաշիվը այլ սարք</translation>
    </message>
    <message>
        <source>Scan the QR code or enter the credentials as text on another device to log in on it.

Attention:
Never show this QR code to anyone else. It would allow unlimited access to your account!</source>
        <translation>Սկանավորեք QR կոդը կամ ներմուծեք տեքստային տվյալները այլ սարքի մեջ՝ մուտք գործելու համար։

Ուշադրություն՝
Այս QR կոդը պետք է հասանելի լինի միայն Ձեզ։ Այն անվերապահորեն թույլատրում է մուտք գործել Ձեր հաշիվ։</translation>
    </message>
    <message>
        <source>Chat address:</source>
        <translation>Զրույցի հասցե:</translation>
    </message>
    <message>
        <source>Password:</source>
        <translation>Գաղտնաբառ:</translation>
    </message>
    <message>
        <source>Hide QR code</source>
        <translation>Թաքցնել QR կոդը</translation>
    </message>
    <message>
        <source>Show as QR code</source>
        <translation>Ցույց տալ QR կոդը</translation>
    </message>
    <message>
        <source>Hide text</source>
        <translation>Թաքցնել տեքստային տվյալները</translation>
    </message>
    <message>
        <source>Show as text</source>
        <translation>Ցույց տալ տեքստային տվյալները</translation>
    </message>
    <message>
        <source>Copy password</source>
        <translation>Պատճենել գաղտնաբառը</translation>
    </message>
    <message>
        <source>Copy chat address</source>
        <translation>Պատճենել զրույցի հասցեն</translation>
    </message>
</context>
<context>
    <name>ChangePassword</name>
    <message>
        <source>Change password</source>
        <translation>Փոխել գաղտնաբառը</translation>
    </message>
    <message>
        <source>Current password:</source>
        <translation>Այժմյա գաղտնաբառ:</translation>
    </message>
    <message>
        <source>New password:</source>
        <translation>Նոր գաղտնաբառ:</translation>
    </message>
    <message>
        <source>New password (repeat):</source>
        <translation>Նոր գաղտնաբառ (կրկին):</translation>
    </message>
    <message>
        <source>New passwords do not match.</source>
        <translation>Նոր գաղտնաբառերը չեն համընկնում։</translation>
    </message>
    <message>
        <source>Current password is invalid.</source>
        <translation>Այժմյա գաղտնաբառը անվավեր է։</translation>
    </message>
    <message>
        <source>After changing your password, you will need to reenter it on all your other devices.</source>
        <translation>Գաղտնաբառը փոխելուց հետո անհրաժեշտ է այն ներմուծել Ձեր մնացած սարքերի մեջ։</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Փոխել</translation>
    </message>
    <message>
        <source>Password changed successfully</source>
        <translation>Գաղտնաբառը հաջողությամբ փոխվել է</translation>
    </message>
    <message>
        <source>Failed to change password: %1</source>
        <translation>Չհաջողվեց փոխել գաղտնաբառը: %1</translation>
    </message>
</context>
<context>
    <name>ChatMessage</name>
    <message>
        <source>Spoiler</source>
        <translation>Գաղտնիք</translation>
    </message>
    <message>
        <source>Download</source>
        <translation>Ներբեռնել</translation>
    </message>
</context>
<context>
    <name>ChatMessageContextMenu</name>
    <message>
        <source>Copy message</source>
        <translation>Պատճենել հաղորդագրությունը</translation>
    </message>
    <message>
        <source>Edit message</source>
        <translation>Ուղղում կատարել</translation>
    </message>
    <message>
        <source>Copy download URL</source>
        <translation>Պատճենել բովանդակության հղումը</translation>
    </message>
    <message>
        <source>Quote message</source>
        <translation>Մեջբերում կատարել</translation>
    </message>
</context>
<context>
    <name>ChatPage</name>
    <message>
        <source>Send a spoiler message</source>
        <translation>Ուղարկել գաղտնի հաղորդագրություն</translation>
    </message>
    <message>
        <source>Unmute notifications</source>
        <translation>Միացնել ծանուցումները</translation>
    </message>
    <message>
        <source>Mute notifications</source>
        <translation>Անջատել ծանուցումները</translation>
    </message>
    <message>
        <source>View profile</source>
        <translation>Զննել պրոֆիլը</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>Որոնել</translation>
    </message>
    <message>
        <source>Search up</source>
        <translation>Որոնել վերևում</translation>
    </message>
    <message>
        <source>Search down</source>
        <translation>Որոնել ներքևում</translation>
    </message>
    <message>
        <source>Close message search bar</source>
        <translation>Ավարտել որոնումը</translation>
    </message>
</context>
<context>
    <name>ChatPageSendingPane</name>
    <message>
        <source>Spoiler hint</source>
        <translation>Գաղտնիքի համատեքստ</translation>
    </message>
    <message>
        <source>Close spoiler hint field</source>
        <translation>Փակել գաղտնիքի համատեքստի դաշտը</translation>
    </message>
    <message>
        <source>Compose message</source>
        <translation>Գրեք հաղորդագրություն</translation>
    </message>
</context>
<context>
    <name>ClientWorker</name>
    <message>
        <source>Your account could not be deleted from the server. Therefore, it was also not removed from this app: %1</source>
        <translation>Չհաջողվեց ջնջել Ձեր հաշիվը սերվերից։ Հետևաբար, այն նաև չի ջնջվել այս հավելվածից: %1</translation>
    </message>
</context>
<context>
    <name>CommonEncoderSettings</name>
    <message>
        <source>Very low</source>
        <translation>Չափազանց ցածր</translation>
    </message>
    <message>
        <source>Low</source>
        <translation>Ցածր</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>Նորմալ</translation>
    </message>
    <message>
        <source>High</source>
        <translation>Բարձր</translation>
    </message>
    <message>
        <source>Very high</source>
        <translation>Շատ բարձր</translation>
    </message>
    <message>
        <source>Constant quality</source>
        <translation>Հաստատուն որակ</translation>
    </message>
    <message>
        <source>Constant bit rate</source>
        <translation>Հաստատուն բիթային հաճախականություն</translation>
    </message>
    <message>
        <source>Average bit rate</source>
        <translation>Միջին բիթային հաճախականություն</translation>
    </message>
    <message>
        <source>Two pass</source>
        <translation>Երկփասս</translation>
    </message>
</context>
<context>
    <name>ConfirmationPage</name>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
</context>
<context>
    <name>ConnectionSettings</name>
    <message>
        <source>Connection settings</source>
        <translation>Կապի կարգավորումներ</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>Փոխել</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Connection settings could not be changed</source>
        <translation>Չհաջողվեց փոփոխել կապի կարգավորումները</translation>
    </message>
    <message>
        <source>Connection settings changed</source>
        <translation>Կապի կարգավորումները պահպանվել են</translation>
    </message>
</context>
<context>
    <name>CustomConnectionSettings</name>
    <message>
        <source>Hostname:</source>
        <translation>Հոսթնեյմ:</translation>
    </message>
    <message>
        <source>The hostname must not contain blank spaces</source>
        <translation>Հոսթնեյմը չի կարող պարունակել դատարկ նշաններ</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Պորտ:</translation>
    </message>
</context>
<context>
    <name>DisablePasswordDisplay</name>
    <message>
        <source>Don&apos;t expose your password in any way</source>
        <translation>Թաքցնել Ձեր գաղտնաբառը ցանկացած դեպքում</translation>
    </message>
    <message>
        <source>Your password will neither be shown as plain text nor included in the login QR code anymore.
You won&apos;t be able to use the login via QR code without entering your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device.</source>
        <translation>Ձեր գաղտնաբառը այլևս չի պատկերվի որպես տեքստ կամ QR կոդ։
Դուք այլևս չեք կարողանա մուտք գործել QR կոդի միջոցով առանց գաղտնաբառ մուտքագրելու, քանի որ այս գործողությունը հնարավոր չէ հետարկել։
Գրի առեք Ձեր գաղտնաբառը այլուր, եթե ցանկանում եք օգտվել Ձեր հաշվից այլ սարքերի միջոցով։</translation>
    </message>
    <message>
        <source>Don&apos;t expose password in any way</source>
        <translation>Թաքցնել գաղտնաբառը ցանկացած դեպքում</translation>
    </message>
</context>
<context>
    <name>DisablePlainTextPasswordDisplay</name>
    <message>
        <source>Don&apos;t show your password</source>
        <translation>Թաքցնել Ձեր գաղտնաբառը</translation>
    </message>
    <message>
        <source>Your password will not be shown anymore but still exposed via the login QR code.
You won&apos;t be able to see your password again because this action cannot be undone!
Consider storing the password somewhere else if you want to use your account on another device without the login QR code.</source>
        <translation>Ձեր գաղտնաբառը չի պատկերվի որպես տեքստ, սակայն QR կոդը կշարունակվի հասանելի լինել։
Դուք չեք կարողանա տեսնել Ձեր գաղտնաբառը, քանի որ այս գործողությունը հնարավոր չէ հետարկել։
Գրի առեք Ձեր գաղտնաբառը այլուր, եթե ցանկանում եք օգտվել Ձեր հաշվից այլ սարքերի միջոցով։</translation>
    </message>
    <message>
        <source>Don&apos;t show password</source>
        <translation>Թաքցնել գաղտնաբառը</translation>
    </message>
</context>
<context>
    <name>DownloadJob</name>
    <message>
        <source>Could not save file: %1</source>
        <translation>Չհաջողվեց պահպանել հետևյալ ֆայլը: %1</translation>
    </message>
    <message>
        <source>Download failed: %1</source>
        <translation>Ներբեռնումը ձախողվել է: %1</translation>
    </message>
</context>
<context>
    <name>EmptyChatPage</name>
    <message>
        <source>Please select a chat to start messaging</source>
        <translation>Ընտրեք զրույց և սկսեք շփվել</translation>
    </message>
</context>
<context>
    <name>GlobalDrawer</name>
    <message>
        <source>About</source>
        <translation>Հավելվածի մասին</translation>
    </message>
    <message>
        <source>Invite friends</source>
        <translation>Հրավիրել ընկերներին</translation>
    </message>
    <message>
        <source>Invitation link copied to clipboard</source>
        <translation>Հրավերի հղումը պատճենվել է</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Կարգավորումներ</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Կապը բացակայում է</translation>
    </message>
    <message>
        <source>Online</source>
        <translation>Կապը հաստատված է</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Կապը հաստատվում է…</translation>
    </message>
    <message>
        <source>Switch device</source>
        <translation>Փոխել սարքը</translation>
    </message>
</context>
<context>
    <name>Kaidan</name>
    <message>
        <source>The link will be opened after you have connected.</source>
        <extracomment>The link is an XMPP-URI (i.e. &apos;xmpp:kaidan@muc.kaidan.im?join&apos; for joining a chat)</extracomment>
        <translation>Հղումը կբացվի կապ հաստատելուց հետո։</translation>
    </message>
</context>
<context>
    <name>LocalAccountRemoval</name>
    <message>
        <source>Remove account from this app</source>
        <translation>Հեռացնել հաշիվը հավելվածից</translation>
    </message>
    <message>
        <source>Your account will be removed from this app.
Your password will be deleted, make sure it is stored in a password manager or you know it!</source>
        <translation>Ձեր հաշիվը հեռացվելու է այս հավելվածից։
Ձեր գաղտնաբառը չի պահպանվելու: Համոզվեք, որ հիշում եք այն կամ այն մատչելի է Ձեզ այլ միջոցներով։</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>Հեռացնել</translation>
    </message>
</context>
<context>
    <name>LoginPage</name>
    <message>
        <source>Log in</source>
        <translation>Մուտք</translation>
    </message>
    <message>
        <source>Log in to your XMPP account</source>
        <translation>Մուտք գործեք Ձեր XMPP հաշիվ</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Կապը հաստատվում է…</translation>
    </message>
    <message>
        <source>Connection settings</source>
        <translation>Կապի կարգավորումներ</translation>
    </message>
</context>
<context>
    <name>LoginSettingsPage</name>
    <message>
        <source>Connection settings</source>
        <translation>Կապի կարգավորումներ</translation>
    </message>
    <message>
        <source>The connection settings will be saved permanently after the first successful login. If they don&apos;t work, you&apos;ll get back to the login page.</source>
        <translation>Կապի կարգավորումները կպահպանվեն առաջին հաջող մուտքից հետո։ Անվավեր կարգավորումներ ներկայացնելու դեպքում Ձեզ կրկին կառաջարկվի լրացնել մուտքի տվյալները։</translation>
    </message>
    <message>
        <source>Enable custom connection settings</source>
        <translation>Միացնել լրացոչցիչ կապի կարգավորումները</translation>
    </message>
    <message>
        <source>Port:</source>
        <translation>Պորտ:</translation>
    </message>
    <message>
        <source>Hostname:</source>
        <translation>Հոսթնեյմ:</translation>
    </message>
    <message>
        <source>Reset</source>
        <translation>Զրոյացնել</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation>Հաստատել</translation>
    </message>
</context>
<context>
    <name>MediaRecorder</name>
    <message>
        <source>Default</source>
        <translation>Լռելյայն</translation>
    </message>
</context>
<context>
    <name>MediaUtils</name>
    <message>
        <source>Take picture</source>
        <translation>Լուսանկարել</translation>
    </message>
    <message>
        <source>Record video</source>
        <translation>Ձայնագրել տեսաֆիլմ</translation>
    </message>
    <message>
        <source>Record voice</source>
        <translation>Ձայնագրել աուդիո</translation>
    </message>
    <message>
        <source>Send location</source>
        <translation>Ուղարկել տեղադրություն</translation>
    </message>
    <message>
        <source>Choose file</source>
        <translation>Ուղարկել ֆայլ</translation>
    </message>
    <message>
        <source>Choose image</source>
        <translation>Ընտրել նկար</translation>
    </message>
    <message>
        <source>Choose video</source>
        <translation>Ընտրել տեսաֆայլ</translation>
    </message>
    <message>
        <source>Choose audio file</source>
        <translation>Ընտրել աուդիո ֆայլ</translation>
    </message>
    <message>
        <source>Choose document</source>
        <translation>Ընտրել փաստաթուղթ</translation>
    </message>
    <message>
        <source>All files</source>
        <translation>Բոլոր ֆայլերի տեսակները</translation>
    </message>
    <message>
        <source>Images</source>
        <translation>Նկարներ</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation>Տեսանյութեր</translation>
    </message>
    <message>
        <source>Audio files</source>
        <translation>Աուդիո ֆայլեր</translation>
    </message>
    <message>
        <source>Documents</source>
        <translation>Փաստաթղթեր</translation>
    </message>
    <message>
        <source>%1 (%2)</source>
        <translation>%1 (%2)</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Աուդիո</translation>
    </message>
    <message>
        <source>Document</source>
        <translation>Փաստաթուղթ</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>Տեղադրություն</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Նկար</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Տեսանյութ</translation>
    </message>
    <message>
        <source>File</source>
        <translation>Ֆայլ</translation>
    </message>
</context>
<context>
    <name>Message</name>
    <message>
        <source>Spoiler</source>
        <translation>Գաղտնիք</translation>
    </message>
</context>
<context>
    <name>MessageHandler</name>
    <message>
        <source>Message could not be sent.</source>
        <translation>Հաղորդագրության ուղարկումը ձախողվել է։</translation>
    </message>
    <message>
        <source>Message correction was not successful.</source>
        <translation>Հաղորդագրության ուղղումը ձախողվել է։</translation>
    </message>
</context>
<context>
    <name>MessageModel</name>
    <message>
        <source>Pending</source>
        <translation>Ընթացքում է</translation>
    </message>
    <message>
        <source>Sent</source>
        <translation>Ուղարկված</translation>
    </message>
    <message>
        <source>Delivered</source>
        <translation>Առաքված</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>Ձախողված</translation>
    </message>
</context>
<context>
    <name>MultimediaSettings</name>
    <message>
        <source>Multimedia Settings</source>
        <translation>Մեդիայի Կարգավորումներ</translation>
    </message>
    <message>
        <source>Configure</source>
        <translation>Կարգավորել</translation>
    </message>
    <message>
        <source>Image Capture</source>
        <translation>Լուսանկարում</translation>
    </message>
    <message>
        <source>Audio Recording</source>
        <translation>Աուդիո Ձայնագրում</translation>
    </message>
    <message>
        <source>Video Recording</source>
        <translation>Տեսաֆիլմերի Ձայնագրում</translation>
    </message>
    <message>
        <source>Camera</source>
        <translation>Տեսախցիկ</translation>
    </message>
    <message>
        <source>Audio input</source>
        <translation>Աուդիո մուտք</translation>
    </message>
    <message>
        <source>Container</source>
        <translation>Կոնտեյներ</translation>
    </message>
    <message>
        <source>Image</source>
        <translation>Նկար</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>Աուդիո</translation>
    </message>
    <message>
        <source>Video</source>
        <translation>Տեսանյութ</translation>
    </message>
    <message>
        <source>Codec</source>
        <translation>Կոդեկ</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation>Տարլուծում</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation>Որակ</translation>
    </message>
    <message>
        <source>Sample Rate</source>
        <translation>Նմուշի Հաճախականություն</translation>
    </message>
    <message>
        <source>Frame Rate</source>
        <translation>Կադրերի Հաճախականություն</translation>
    </message>
    <message>
        <source>Ready</source>
        <translation>Պատրաստ է</translation>
    </message>
    <message>
        <source>Initializing…</source>
        <translation>Նախապատրաստում…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Անհասանելի</translation>
    </message>
    <message>
        <source>Recording…</source>
        <translation>Ձայնագրում…</translation>
    </message>
    <message>
        <source>Paused</source>
        <translation>Կանգնեցված</translation>
    </message>
</context>
<context>
    <name>NewMedia</name>
    <message>
        <source>Initializing…</source>
        <translation>Նախապատրաստում…</translation>
    </message>
    <message>
        <source>Unavailable</source>
        <translation>Անհասանելի</translation>
    </message>
    <message>
        <source>Recording… %1</source>
        <translation>Ձայնագրում… %1</translation>
    </message>
    <message>
        <source>Paused %1</source>
        <translation>%1-ը կանգնեցված է</translation>
    </message>
</context>
<context>
    <name>PasswordRemovalPage</name>
    <message>
        <source>Remove password</source>
        <translation>Հեռացնել գաղտնաբառը</translation>
    </message>
    <message>
        <source>You can decide to only not show your password for &lt;b&gt;%1&lt;/b&gt; as text anymore or to remove it completely from the account transfer. If you remove your password completely, you won&apos;t be able to use the account transfer via scanning without entering your password because it is also removed from the QR code.</source>
        <translation>Դուք կարող եք թաքցնել Ձեր տեքստային գաղտնաբառը &lt;b&gt;%1&lt;/b&gt; հաշվի համար կամ այն ամբողջովին հեռացնել հաշվի փոխանցման ընթացքից։ Եթե Դուք ամբողջովին հեռացնեք Ձեր գաղտնաբառը, Դուք ստիպված կլինեք ինքնուրույն ներմուծել այն, քանի որ այն նաև կվերանա QR կոդից։</translation>
    </message>
    <message>
        <source>Do not show password as text</source>
        <translation>Թաքցնել տեքստային գաղտնաբառը</translation>
    </message>
    <message>
        <source>Remove completely</source>
        <translation>Թաքցնել ամբողջովին</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Mark as read</source>
        <translation>Համարել կարդացած</translation>
    </message>
    <message>
        <source>Available</source>
        <translation>Հասանելի</translation>
    </message>
    <message>
        <source>Free for chat</source>
        <translation>Ցանկանում եմ զրուցել</translation>
    </message>
    <message>
        <source>Away</source>
        <translation>Բացակա</translation>
    </message>
    <message>
        <source>Do not disturb</source>
        <translation>Չանհանգստացնել</translation>
    </message>
    <message>
        <source>Away for longer</source>
        <translation>Բացակա երկար ժամանակով</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Անհասանելի</translation>
    </message>
</context>
<context>
    <name>QmlUtils</name>
    <message>
        <source>Invalid username or password.</source>
        <translation>Անվավեր օգտանուն կամ գաղտնաբառ։</translation>
    </message>
    <message>
        <source>Cannot connect to the server. Please check your internet connection.</source>
        <translation>Չհաջողվեց կապ հաստատել սերվերի հետ։ Համոզվեք, որ ինտերնետ կապը միացված է։</translation>
    </message>
    <message>
        <source>The server doesn&apos;t support secure connections.</source>
        <translation>Սերվերը չի սպասարկում անվտանգ կապեր։</translation>
    </message>
    <message>
        <source>Error while trying to connect securely.</source>
        <translation>Անվտանգ կապ հաստատելը ձախողվել է։</translation>
    </message>
    <message>
        <source>Authentification protocol not supported by the server.</source>
        <translation>Նույնականացման մեթոդը չի սպասարկվում սերվերի կողմից։</translation>
    </message>
    <message>
        <source>This server does not support registration.</source>
        <translation>Այս սերվերը չի սպասարկում գրանցումներ։</translation>
    </message>
    <message>
        <source>The server is offline or blocked by a firewall.</source>
        <translation>Այս սերվերը անցանց է կամ կասեցված հրապատի կողմից։</translation>
    </message>
    <message>
        <source>The connection could not be refreshed.</source>
        <translation>Չհաջողվեց թարմացնել կապը։</translation>
    </message>
    <message>
        <source>The internet access is not permitted. Please check your system&apos;s internet access configuration.</source>
        <translation>Չհաջողվեց հաստատել ինտերնետային կապ։ Ստուգեք Ձեր ինտերնետի կարգավորումները։</translation>
    </message>
    <message>
        <source>Could not connect to the server. Please check your internet connection or your server name.</source>
        <translation>Չհաջողվեց կապ հաստատել սերվերի հետ։ Ստուգեք Ձեր ինտերնետի կարգավորումները կամ Ձեր սերվերի անվանումը։</translation>
    </message>
    <message>
        <source>%1 is online</source>
        <translation>%1-ը հասանելի է</translation>
    </message>
    <message>
        <source>%1 is typing…</source>
        <translation>%1-ը հավաքում է…</translation>
    </message>
    <message>
        <source>%1 paused typing</source>
        <translation>%1-ը դադարեց հավաքել</translation>
    </message>
</context>
<context>
    <name>QrCodeGenerator</name>
    <message>
        <source>Generating the QR code failed: %1</source>
        <translation>QR կոդի ստեղծումը ձախողվել է: %1</translation>
    </message>
</context>
<context>
    <name>QrCodeOnboardingPage</name>
    <message>
        <source>Scan QR code</source>
        <translation>Սկանավորել QR կոդ</translation>
    </message>
    <message>
        <source>Show explanation</source>
        <translation>Ցույց տալ բացատրությունը</translation>
    </message>
    <message>
        <source>Continue without QR code</source>
        <translation>Շարունակել առանց QR կոդի</translation>
    </message>
    <message>
        <source>Scan the QR code from your existing device to transfer your account.</source>
        <translation>Սկանավորեք QR կոդը արդեն մուտք գործած սարքից՝ հաշիվը փոխանցելու համար։</translation>
    </message>
    <message>
        <source>Connecting…</source>
        <translation>Կապը հաստատվում է…</translation>
    </message>
    <message>
        <source>This QR code is not a valid login QR code.</source>
        <translation>Այս QR կոդը անվավեր է մուտքի համար։</translation>
    </message>
</context>
<context>
    <name>QrCodeScanner</name>
    <message>
        <source>The camera format &apos;%1&apos; is not supported.</source>
        <translation>«%1» տեսախցիկի ֆորմատը չի սպասարկվում։</translation>
    </message>
    <message>
        <source>There is no camera available.</source>
        <translation>Հասանելի տեսախցիկ չկա։</translation>
    </message>
    <message>
        <source>Your camera is busy.
Try to close other applications using the camera.</source>
        <translation>Ձեր տեսախցիկը օգտագործվում է։
Փակեք այն հավելվածները, որոնք օգտագործում են այն։</translation>
    </message>
</context>
<context>
    <name>RegistrationLoginDecisionPage</name>
    <message>
        <source>Set up</source>
        <translation>Կարգավորում</translation>
    </message>
    <message>
        <source>Register a new account</source>
        <translation>Ստեղծել նոր հաշիվ</translation>
    </message>
    <message>
        <source>Use an existing account</source>
        <translation>Մուտք գործել</translation>
    </message>
</context>
<context>
    <name>RemoteAccountDeletion</name>
    <message>
        <source>Delete account completely</source>
        <translation>Ջնջել հաշիվը ամբողջովին</translation>
    </message>
    <message>
        <source>Your account will be deleted completely, which means from this app and from the server.
You will not be able to use your account again!</source>
        <translation>Ձեր հաշիվը ամբողջովին կջնջվի, ինչը նշանակում է այս հավելվածից և սերվերից։
Կրկին այս հաշվից օգտվելը հնարավոր չի լինի։</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Ջնջել</translation>
    </message>
</context>
<context>
    <name>RosterAddContactSheet</name>
    <message>
        <source>Add new contact</source>
        <translation>Ստեղծել նոր կոնտակտ</translation>
    </message>
    <message>
        <source>This will also send a request to access the presence of the contact.</source>
        <translation>Այս գործողությունը նաև ներկայության զննման առաջարկ կուղարկի։</translation>
    </message>
    <message>
        <source>Jabber-ID:</source>
        <translation>Jabber-ID:</translation>
    </message>
    <message>
        <source>user@example.org</source>
        <translation>user@example.org</translation>
    </message>
    <message>
        <source>Nickname:</source>
        <translation>Անվանում:</translation>
    </message>
    <message>
        <source>Optional message:</source>
        <translation>Լրացուցիչ հաղորդագրություն:</translation>
    </message>
    <message>
        <source>Tell your chat partner who you are.</source>
        <translation>Ասացեք Ձեր զրուցակցին, թե ով եք Դուք:</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Add</source>
        <translation>Ավելացնել</translation>
    </message>
</context>
<context>
    <name>RosterManager</name>
    <message>
        <source>Could not add contact, as a result of not being connected.</source>
        <translation>Չհաջողվեց ստեղծել կոնտակտը, քանի որ կապը բացակայում է։</translation>
    </message>
    <message>
        <source>Could not remove contact, as a result of not being connected.</source>
        <translation>Չհաջողվեց ջնջել կոնտակտը, քանի որ կապը բացակայում է։</translation>
    </message>
    <message>
        <source>Could not rename contact, as a result of not being connected.</source>
        <translation>Չհաջողվեց վերանվանել կոնտակտը, քանի որ կապը բացակայում է։</translation>
    </message>
</context>
<context>
    <name>RosterPage</name>
    <message>
        <source>Connecting…</source>
        <translation>Կապը հաստատվում է…</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation>Կոնտակտներ</translation>
    </message>
    <message>
        <source>Add new contact</source>
        <translation>Նոր կոնտակտ</translation>
    </message>
    <message>
        <source>Offline</source>
        <translation>Անցանց</translation>
    </message>
    <message>
        <source>Search contacts</source>
        <translation>Փնտրել կոնտակտներ</translation>
    </message>
</context>
<context>
    <name>RosterRemoveContactSheet</name>
    <message>
        <source>Delete contact</source>
        <translation>Ջնջել կոնտակտը</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>Ջնջել</translation>
    </message>
    <message>
        <source>Do you really want to delete the contact &lt;b&gt;%1&lt;/b&gt; from your contact list?</source>
        <translation>Դուք իսկապե՞ս ցանկանում եք հեռացնել &lt;b&gt;%1&lt;/b&gt; կոնտակտը Ձեր ցուցակից։</translation>
    </message>
</context>
<context>
    <name>RosterRenameContactSheet</name>
    <message>
        <source>Rename contact</source>
        <translation>Վերանվանել կոնտակտը</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>Վերանվանել</translation>
    </message>
</context>
<context>
    <name>SendMediaSheet</name>
    <message>
        <source>Caption</source>
        <translation>Համատեքստ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>Չեղարկել</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>Ուղարկել</translation>
    </message>
</context>
<context>
    <name>ServerListModel</name>
    <message>
        <source>Custom server</source>
        <translation>Այլ սերվեր</translation>
    </message>
    <message>
        <source>No limitation</source>
        <extracomment>Unlimited file size for uploading files
----------
Deletion of message history saved on server</extracomment>
        <translation>Անսահմանափակ</translation>
    </message>
    <message>
        <source>%1 days</source>
        <translation>%1 օր</translation>
    </message>
</context>
<context>
    <name>SettingsContent</name>
    <message>
        <source>Change password</source>
        <translation>Փոխել գաղտնաբառը</translation>
    </message>
    <message>
        <source>Changes your account&apos;s password. You will need to re-enter it on your other devices.</source>
        <translation>Փոխում է Ձեր հաշվի գաղտնաբառը։ Դուք պետք է կրկին մուտքագրեք այն այլ սարքերի մեջ։</translation>
    </message>
    <message>
        <source>Multimedia Settings</source>
        <translation>Մեդիա Կարգավորումներ</translation>
    </message>
    <message>
        <source>Configure photo, video and audio recording settings</source>
        <translation>Փոփոխել ֆոտո, վիդեո և աուդիո ձայնագրման կարգավորումները</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Կարգավորումներ</translation>
    </message>
    <message>
        <source>Account security</source>
        <translation>Հաշվի անվտանգություն</translation>
    </message>
    <message>
        <source>Remove account from Kaidan</source>
        <translation>Հեռացնել հաշիվը Kaidan-ից</translation>
    </message>
    <message>
        <source>Remove account from this app</source>
        <translation>Հեռացնել հաշիվը հավելվածից</translation>
    </message>
    <message>
        <source>Delete account</source>
        <translation>Ջնջել հաշիվը</translation>
    </message>
    <message>
        <source>Delete account from the server</source>
        <translation>Հեռացնել հաշիվը սերվերից</translation>
    </message>
    <message>
        <source>Connection Settings</source>
        <translation>Կապի Կարգավորումներ</translation>
    </message>
    <message>
        <source>Configure the hostname and port to connect to</source>
        <translation>Սահմանել հոսթնեյմը և պորտը</translation>
    </message>
    <message>
        <source>Configure whether this device can be used to log in on another device</source>
        <translation>Սահմանել, կարող է արդյոք այս սարքը օգտագործվել այլ սարքերով մուտք գործելու համար</translation>
    </message>
</context>
<context>
    <name>SettingsPage</name>
    <message>
        <source>Settings</source>
        <translation>Կարգավորումներ</translation>
    </message>
</context>
<context>
    <name>StartPage</name>
    <message>
        <source>Enjoy free communication on every device!</source>
        <translation>Վայելեք ազատ շփումը ցանկացած սարքով։</translation>
    </message>
    <message>
        <source>Let&apos;s start</source>
        <translation>Սկսել</translation>
    </message>
</context>
<context>
    <name>SubRequestAcceptSheet</name>
    <message>
        <source>Subscription Request</source>
        <translation>Բաժանորդագրության Առաջարկ</translation>
    </message>
    <message>
        <source>You received a subscription request by &lt;b&gt;%1&lt;/b&gt;. If you accept it, the account will have access to your presence status.</source>
        <translation>Դուք ստացել եք բաժանորդագրության առաջարկ &lt;b&gt;%1&lt;/b&gt;-ից։ Եթե Դուք համաձայնեք, ապա հաշիվը կկարողանա զննել Ձեր ներկայությունը։</translation>
    </message>
    <message>
        <source>Decline</source>
        <translation>Մերժել</translation>
    </message>
    <message>
        <source>Accept</source>
        <translation>Համաձայնել</translation>
    </message>
</context>
<context>
    <name>UploadManager</name>
    <message>
        <source>Could not send file, as a result of not being connected.</source>
        <translation>Չհաջողվեց ուղարկել ֆայլը, քանի որ կապը բացակայում է։</translation>
    </message>
    <message>
        <source>Message could not be sent.</source>
        <translation>Հաղորդագրության ուղարկումը ձախողվել է։</translation>
    </message>
</context>
<context>
    <name>UserProfilePage</name>
    <message>
        <source>Profile</source>
        <translation>Պրոֆիլ</translation>
    </message>
    <message>
        <source>Rename contact</source>
        <translation>Վերանվանել կոնտակտը</translation>
    </message>
    <message>
        <source>Show QR code</source>
        <translation>Ցույց տալ QR կոդը</translation>
    </message>
    <message>
        <source>Remove contact</source>
        <translation>Ջնջել կոնտակտը</translation>
    </message>
    <message>
        <source>Online devices</source>
        <translation>Հասանելի սարքեր</translation>
    </message>
</context>
<context>
    <name>VCardModel</name>
    <message>
        <source>Name</source>
        <translation>Անուն</translation>
    </message>
    <message>
        <source>Nickname</source>
        <translation>Անվանում</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Անձի մասին</translation>
    </message>
    <message>
        <source>Email</source>
        <translation>Էլ․ հասցե</translation>
    </message>
    <message>
        <source>Birthday</source>
        <translation>Ծննդյան ամսաթիվ</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>Վեբ-կայք</translation>
    </message>
</context>
</TS>
